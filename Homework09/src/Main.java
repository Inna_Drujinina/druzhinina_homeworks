public class Main {

    public static void main(String[] args) {
        Figure figure = new Figure(3,11);
	    Ellipse ellipse = new Ellipse(6,3);
        Circle circle = new Circle(4);
        Rectangle rectangle = new Rectangle(8.4, 4.9);
        Square square = new Square(9.36);

        System.out.println("Периметр фигуры = " + figure.getPerimeter() + " (условных ед.)");
        System.out.println("Периметр эллипса = " + ellipse.getPerimeter() + " (условных ед.)");
        System.out.println("Периметр круга = " + circle.getPerimeter() + " (условных ед.)");
        System.out.println("Периметр прямоугольника = " + rectangle.getPerimeter() + " (условных ед.)");
        System.out.println("Периметр квадрата = " + square.getPerimeter() + " (условных ед.)");

    }
}
